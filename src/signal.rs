use std::mem::transmute;

use wlroots_sys::{
    wl_signal,
};
use wayland_sys::server::{
    signal::rust_listener_get_user_data,
    wl_listener,
};

use crate::resource::Resource;

//pub struct Callback<F> {
    ////where F: FnMut(T), {
    //pub callback: Box<F>,
//}
pub struct Callback<T> {
    //where F: FnMut(T), {
    pub callback: Box<dyn FnMut(Resource<T>)>,
}

//impl<F> Callback<F>  {
    //where F: FnMut(T), {
    //pub fn new<F>(callback: F) -> Callback<F> {
        //Callback {
            //callback: Box::new(callback),
        //}
    //}
//}
impl<T> Callback<T> {
    //where F: Box<fn(T), {
    //where F: FnMut(T), {
    //pub fn new(callback: Box<dyn FnMut(Resource<T>)>) -> Callback<T> {
    pub fn new(callback: Box<dyn FnMut(Resource<T>)>) -> Callback<T> {
        Callback {
            callback: callback,
        }
    }

    pub extern "C" fn callback_raw(listener: *mut wl_listener, data: *mut libc::c_void) {
        unsafe {
            println!("start");
            let callback_box = rust_listener_get_user_data(
                transmute::<*mut wl_listener, *mut wayland_sys::server::wl_listener>(listener));
            let callback = Box::from_raw(callback_box);
            let resource = Resource::<T>::from_raw_ptr(data);

            println!("yeah!!!");

            callback(resource);
        }
    }
}
