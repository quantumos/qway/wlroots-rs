use std::mem::transmute;

use wlroots_sys::{
    wlr_backend,
    wlr_renderer_create_func_t,
    wlr_backend_autocreate,
    wl_signal,
    server::WAYLAND_SERVER_HANDLE,
};
use wayland_sys::server::signal::{
    rust_listener_create,
    rust_listener_set_user_data,
    wl_signal_add,
};

use crate::{
    resource::Resource,
    display::Display,
    input::Device,
    signal::Callback,
};
//
//use crate::display;
//use wayland_sys::server::WAYLAND_SERVER_HANDLE;
//use wayland_sys::server::signal::wl_signal_add;
//use crate::input;
//use crate::output;

pub struct Backend {
    resource: Resource<wlr_backend>,
    //callback_input_create: Callback<Box<dyn FnMut(Device)>>,
    callback_input_create: Callback<Device>,
}

    //pub fn set_output_create_callback<F>(&self, callback: F)
    //where F: Fn(output::Output), {
        //let callback_box = Box::new(callback);
        //let listener = wayland_sys::server::signal::rust_listener_create(output::Output::raw_callback::<F>);

        //unsafe {
            //wayland_sys::server::signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            //let wlr_signal = (*self.backend).events.new_output;
            //let mut wl_signal =
                //transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            //wl_signal_add(&mut wl_signal, listener);
        //}
    //}
impl Backend {
    pub fn new(display: &mut Display, func: wlr_renderer_create_func_t) -> Backend {
        Backend {
            resource: Resource::new(unsafe {
                println!("doing r1");
                //wlr_backend_autocreate(transmute::<wl_display, wlroots_sys::wl_display>(display.get_raw_ptr()), func)
                wlr_backend_autocreate(display.get_raw_ptr() as *mut _, func)
            }),
            callback_input_create: Callback::new(
                                       Box::new(|device| {println!("Input create called!!!");})),
        }
    }

    pub fn register_callbacks(&self) {
        self.register_callback(&self.callback_input_create, 
            (*self.resource).events.new_input);
    }

    fn register_callback<F>(&self, callback: F, signal: wl_signal) {
        let callback_box = Box::new(callback);
        let listener = rust_listener_create(Callback::<F>::callback_raw);

        unsafe {
            println!("doing r2");
            rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            let mut wl_signal = transmute::<wl_signal, wayland_sys::server::wl_signal>(signal);
            wl_signal_add(&mut wl_signal, listener);
            println!(":(");
            println!("done r2");
        }
    }

    pub fn start(&mut self) -> bool {
        unsafe {
            wlroots_sys::wlr_backend_start(&mut *self.resource)
        }
    }
}
//pub struct Backend {
    //pub backend: *mut wlroots_sys::wlr_backend,
    //pub new_output_notify: |
//}

//impl Backend {
    //pub fn create(display: &display::Display, create_renderer_func: wlroots_sys::wlr_renderer_create_func_t) -> Backend {
        //unsafe {
            //Backend {
                //backend: wlroots_sys::wlr_backend_autocreate(display.display, create_renderer_func)
            //}
        //}
    //}

    //pub fn start(&self) -> bool {
        //unsafe {
            //wlroots_sys::wlr_backend_start(self.backend)
        //}
    //}

    ////pub fn set_new_output_callback(&self, notify_func: wayland_sys::server::wl_notify_func_t) {
        ////let listener = wayland_sys::server::signal::rust_listener_create(notify_func);

        ////unsafe  {
            ////let wlr_signal = (*self.backend).events.new_output;
            ////let mut wl_signal = 
                ////transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            ////wl_signal_add(&mut wl_signal, listener);
        ////}
    ////}

    ////pub fn set_new_input_callback(&self, notify_func: wayland_sys::server::wl_notify_func_t) {
        ////let listener = wayland_sys::server::signal::rust_listener_create(notify_func);

        ////unsafe  {
            ////let wlr_signal = (*self.backend).events.new_input;
            ////let mut wl_signal = 
                ////transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            ////wl_signal_add(&mut wl_signal, listener);
        ////}
    ////}

    //pub fn set_output_create_callback<F>(&self, callback: F)
    //where F: Fn(output::Output), {
        //let callback_box = Box::new(callback);
        //let listener = wayland_sys::server::signal::rust_listener_create(output::Output::raw_callback::<F>);

        //unsafe {
            //wayland_sys::server::signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            //let wlr_signal = (*self.backend).events.new_output;
            //let mut wl_signal =
                //transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            //wl_signal_add(&mut wl_signal, listener);
        //}
    //}

    //pub fn set_output_frame_callback<F>(&self, callback: F)
    //where F: Fn(output::Output), {
        //let callback_box = Box::new(callback);
        //let listener = wayland_sys::server::signal::rust_listener_create(output::Output::raw_callback::<F>);

        //unsafe {
            //wayland_sys::server::signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            //let wlr_signal = (*self.backend).events.new_output;
            //let mut wl_signal =
                //transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            //wl_signal_add(&mut wl_signal, listener);
        //}
    //}

    //pub fn set_input_create_callback<F>(&self, callback: F)
    //where F: Fn(input::Device), {
        //let callback_box = Box::new(callback);
        //let listener = wayland_sys::server::signal::rust_listener_create(input::Device::raw_callback::<F>);

        //unsafe {
            //wayland_sys::server::signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            //let wlr_signal = (*self.backend).events.new_input;
            //let mut wl_signal =
                //transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            //wl_signal_add(&mut wl_signal, listener);
        //}
    //}

    ////pub fn set_input_destroy_callback<F>(&self, callback: F)
    ////where F: Fn(input::Device), {
        ////let callback_box = Box::new(callback);
        ////let listener = wayland_sys::server::signal::rust_listener_create(input::Device::raw_callback::<F>);

        ////unsafe {
            ////wayland_sys::server::signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            ////let wlr_signal = (*self.backend).events.new_input;
            ////let mut wl_signal =
                ////transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            ////wl_signal_add(&mut wl_signal, listener);
        ////}
    ////}
    ////pub fn on_new_input<F>(&self, callback: F) {
        ////let notify_func = 
    ////pub fn add_signal_callback(mut signal: wayland_sys::server::wl_signal, notify_func: wayland_sys::server::wl_notify_func_t) {
    ////pub fn add_signal_callback(wlr_signal: wlroots_sys::wl_signal, notify_func: wayland_sys::server::wl_notify_func_t) {
    ////pub fn add_signal_callback(&self, signal: Signal, notify_func: wayland_sys::server::wl_notify_func_t) {
        ////let listener = wayland_sys::server::signal::rust_listener_create(notify_func);
        ////println!("{:?}", signal);
        ////let wlr_signal = match signal {
            ////CreateOutput => unsafe { (*self.backend).events.new_output },
            ////CreateInput => unsafe { (*self.backend).events.new_input },
            ////_ => unsafe { (*self.backend).events.new_input },
        ////};
        //////let wlr_signal = unsafe { (*self.backend).events.new_input };

        ////let mut wl_signal = unsafe {
            ////transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal)
        ////};
        ////unsafe {
            ////wl_signal_add(&mut wl_signal, listener);
        ////}
    ////}
    //////
    ////pub fn add_signal_callback_old(wlr_signal: wlroots_sys::wl_signal, notify_func: wayland_sys::server::wl_notify_func_t) {
    //////pub fn add_signal_callback(&self, signal: Signal, notify_func: wayland_sys::server::wl_notify_func_t) {
        ////let listener = wayland_sys::server::signal::rust_listener_create(notify_func);
        //////let wlr_signal = unsafe {
            //////match signal {
                //////CreateOutput => (*self.backend).events.new_output,
                //////CreateInput => (*self.backend).events.new_input,
                //////_ => (*self.backend).events.new_output,
            //////}
        //////};

        ////let mut wl_signal = unsafe {
            ////transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal)
        ////};
        ////unsafe {
            ////wl_signal_add(&mut wl_signal, listener);
        ////}
    ////}
//}

//impl Drop for Backend {
    //fn drop(&mut self) {
        //unsafe {
            //wlroots_sys::wlr_backend_destroy(self.backend);
        //}
    //}
//}
