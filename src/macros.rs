// Gets the parent struct from a pointer
// VERY unsafe. the parent struct _must_ be repr(C), and the
// type passed to this macro _must_ match the type of the parent.
macro_rules! container_of(
    ($ptr: expr, $container: ty, $field: ident) => {
        ($ptr as *mut u8).offset(-(offset_of!($container, $field) as isize)) as *mut $container
    }
);
