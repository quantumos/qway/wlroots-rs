use std::ops::Deref;

use wlroots_sys::server::{
    wl_display,
    WAYLAND_SERVER_HANDLE,
};

use crate::resource::Resource;

pub struct Display {
    resource: Resource<wl_display>,
}

impl Display {
    pub fn new() -> Display {
        Display {
            resource: Resource::new(unsafe {
                ffi_dispatch!(WAYLAND_SERVER_HANDLE, wl_display_create,) as *mut wl_display
            }),
        }
    }

    pub fn get_raw_ptr(&mut self) -> *mut wl_display {
        &mut *self.resource
    }
}

//impl Deref for Display {
    //type Target = Display;
    
    //fn deref(&self) -> &Resource<Display> {
        //*self.resource
    //}
//}

//pub struct Display {
    //pub display: *mut wlroots_sys::wl_display,
//}

//impl Display {
    //pub fn create() -> Display {
        //unsafe {
            //Display {
                //display: ffi_dispatch!(WAYLAND_SERVER_HANDLE, wl_display_create,) as *mut wlroots_sys::wl_display
            //}
        //}
    //}

    //pub fn run(&self) {
        //unsafe {
            //ffi_dispatch!(WAYLAND_SERVER_HANDLE, wl_display_run, self.display as *mut _);
        //}
    //}
//}

//impl Drop for Display {
    //fn drop(&mut self) {
        //unsafe {
            //ffi_dispatch!(WAYLAND_SERVER_HANDLE, wl_display_destroy, self.display as *mut _);
        //}
    //}
//}
