use wlroots_sys::wlr_log_func_t;

pub enum LogLevel {
    Silent = 0,
    Error = 1,
    Info = 2,
    Debug = 3,
}

pub struct Logger {}

impl Logger {
    pub fn init(verbosity: LogLevel, callback: wlr_log_func_t) {
        unsafe {
            wlroots_sys::wlr_log_init(verbosity as u32, callback);
        }
    }
}
