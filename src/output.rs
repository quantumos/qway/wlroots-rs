use wayland_sys::server::signal;
use std::mem::transmute;

pub struct Output {
    output: *mut wlroots_sys::wlr_output,
}

impl Output {
    pub unsafe fn from_void_data(data: *mut libc::c_void) -> Output {
        Output {
            output: transmute::<*mut libc::c_void, *mut wlroots_sys::wlr_output>(data),
        }
    }

    pub fn get_raw_ptr(&self) -> *mut wlroots_sys::wlr_output {
        self.output
    }

    pub extern "C" fn raw_callback<F>(listener: *mut wayland_sys::server::wl_listener, data: *mut libc::c_void)
    where F: Fn(Output) {
        unsafe {
            let callback_raw = signal::rust_listener_get_user_data(listener);
            let callback_box = Box::from_raw(callback_raw as *mut F);
            let device = Output::from_void_data(data);
            callback_box(device);
        }
    }

    pub fn set_output_frame_callback<F>(&self, callback: F)
    where F: Fn(Output), {
        let callback_box = Box::new(callback);
        let listener = signal::rust_listener_create(Output::raw_callback::<F>);

        unsafe {
            signal::rust_listener_set_user_data(listener, Box::into_raw(callback_box) as *mut _);

            let wlr_signal = (*self.output).events.frame;
            let mut wl_signal =
                transmute::<wlroots_sys::wl_signal, wayland_sys::server::wl_signal>(wlr_signal);
            println!("test");
            signal::wl_signal_add(&mut wl_signal, listener);
        }
    }
}
