use wayland_sys::server::signal;
use std::mem::transmute;
use xkbcommon::xkb;

#[derive(FromPrimitive)]
#[derive(Debug)]
pub enum Type {
    Keyboard = 0,
    Pointer = 1,
    Touch = 2,
    TabletTool = 3,
    TabletPad = 4,
    Switch = 5,
}

pub struct Device {
    device: *mut wlroots_sys::wlr_input_device,
}

impl Device {
    pub unsafe fn from_void_data(data: *mut libc::c_void) -> Device {
        Device {
            device: transmute::<*mut libc::c_void, *mut wlroots_sys::wlr_input_device>(data),
        }
    }

    pub fn get_raw_ptr(&self) -> *mut wlroots_sys::wlr_input_device {
        self.device
    }

    pub fn get_type(&self) -> Option<Type> {
        num::FromPrimitive::from_u32(unsafe {
            (*(*&self.device)).type_
        })
    }
    
    pub fn get_keyboard_raw_ptr(&self) -> *mut wlroots_sys::wlr_keyboard {
        unsafe {
            (*(*&self.device)).__bindgen_anon_1.keyboard
        }
    }

    pub fn get_keyboard(&self) -> Option<Keyboard> {
        match self.get_type().unwrap() {
            Type::Keyboard => {
                Some(Keyboard::from_raw_ptr(self.get_keyboard_raw_ptr()))
            },
            _ => None,
        }
    }

    pub extern "C" fn raw_callback<F>(listener: *mut wayland_sys::server::wl_listener, data: *mut libc::c_void) 
    where F: Fn(Device) {
        unsafe {
            let callback_raw = signal::rust_listener_get_user_data(listener);
            let callback_box = Box::from_raw(callback_raw as *mut F);
            let device = Device::from_void_data(data);
            callback_box(device);
        }
    }
}

pub struct Keyboard {
    keyboard: *mut wlroots_sys::wlr_keyboard,
}

impl Keyboard {
    pub fn from_raw_ptr(ptr: *mut wlroots_sys::wlr_keyboard) -> Keyboard {
        Keyboard {
            keyboard: ptr,
        }
    }

    pub fn get_raw_ptr(&self) -> *mut wlroots_sys::wlr_keyboard {
        self.keyboard
    }

    pub fn set_keymap(&self, keymap: &xkb::Keymap) {
        unsafe {
            wlroots_sys::wlr_keyboard_set_keymap(self.keyboard, keymap.get_raw_ptr() as _);
        }
    }
}
