use std::mem::transmute;
use std::ops::{Deref, DerefMut};

pub struct Resource<T> {
    data: *mut T,
    handle: bool,
}

impl<T> Resource<T> {
    pub fn new(data: *mut T) -> Resource<T> {
        Resource {
            data: data,
            handle: true,
        }
    }

    pub fn release(&mut self) {
        self.handle = false;
    }

    pub unsafe fn from_raw_ptr(data: *mut libc::c_void) -> Resource<T> {
        Resource {
            data: transmute::<*mut libc::c_void, *mut T>(data),
            handle: true,
        }
    }
}

impl<T> Deref for Resource<T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe {
            &*self.data
        }
    }
}

impl<T> DerefMut for Resource<T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe {
            &mut *self.data
        }
    }
}
